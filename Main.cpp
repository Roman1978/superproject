#include <iostream>
#include "Helpers.h"

int main()
{ 
	int a;
	int b;
	std::cout << "Enter the first number: ";
	std::cin >> a;

	std::cout << "Enter the second number: ";
	std::cin >> b;

	int result = sumpow2(a, b);
	std::cout << "Result = " << result << std::endl;
}